console.log("hello")

// SECTION-JSON Objects
/*
	JSON stands for Javascript Object Notation
	JSON is also used on other programming language, hence the "Notation" in its name
	-JSON is not to be confused with Javascript objects
*/

/*let cities=[
	{ 
		city: "Cebu",
	  	province: "Cebu",
	  	country: "Philippines"
	},
	{ 
		city: "Machida",
	  	province: "Tokyo",
	  	country: "Japan"
	},
	{ 
		city: "Manila",
	  	province: "Manila",
	  	country: "Philippines"
	}

	]
console.log(cities)
*/

/*
	below is an example of JSON
	-when logged in the console, it seems that the appearance of JSON does not differ from JS Object. But since JSON is used by other languages as well. it is common to see it as JS object since it is the blueprint  of the JSON format
*/
/*
	WHAT JSON DOES
		-JSON is used for serializing data types into bytes
		-bytes is a unit of data composed of eight binary digits(1/0) used to represent a character(letters, numbers, typographical symbol)
		-serialization is the process of converting data into a series of bytes for easier transfer of information
*/
let cities=[
	{ 
		"city": "Cebu",
	  	"province": "Cebu",
	  	"country": "Philippines"
	},
	{ 
		"city": "Machida",
	  	"province": "Tokyo",
	  	"country": "Japan"
	},
	{ 
		"city": "Manila",
	  	"province": "Manila",
	  	"country": "Philippines"
	}

	]
console.log(cities)

// JSON Methods
	// JSON objects contains methods for parsing and converting data into/from JSON or stringgified method

// SECTION - stringify method
/*
	-stringified JSON is a JS object converted into a string (but in JSON format) to be used in other functions of a JS application
	-they commonly used in HTTP requests where information is required to be sent  and received in a stringified JSON format
	- request are an important part of programming where an application communicates with a bakend application to perform different tasks such as getting/creating data in a data base
	-a front end application is an application that is used to interact with user to perform tasks and display information. While the backend application are commonly used for business logic and data processing

	- database normally stores information/data that can be used in most application
	-commonly strored data in databases are user information, transaction records, and product information
	-Node/express JS are two of technologies that are used for creating backend applications which process request from frontend application
	-Node JS is a Java Runtime Environment(JRE)/ software that is made to execute other software
	-Express JS is a Node JS framework that provides features for easily creating web and mobile applications

*/
let batchesArr = [{batchname: "Batch X"}, {batchName: "Batch Y"}];
console.log(batchesArr);
// stringify method-used to convert JS objects into a string
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));

// direct conversion of data type from JS obj to stringified JSON
let data =JSON.stringify({
	name: "Strange",
	age :25,
	address:{
		city: "Cebu",
		country: "Philippines"
	}
});
console.log(data)

// an example where JSON is commonly used is on package.json files which is an express JS application  uses to keep track of the information regarding a repository/project (see package.json)


let fname=prompt("What is your first name?");
let lname=prompt("What is your last name?");
let age=prompt("What is your age?");
let address={
	city:prompt("Which city do you live?:"),
	country:prompt("Which country does your city belong?")
}

// JSON.stringify with variables and prompt()
let otherData = JSON.stringify({
	fname:fname,
	lname: lname,	
	age: age,
	address:address
})

console.log(otherData)


// parse Method
/*
	-converts the stringyfied JSON into javascrip Objects
	-Objects are common data types used in applications because of the complex data structures  that can be created out of them
*/
let batchesJSON = `[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON))

console.log(JSON.parse(otherData))